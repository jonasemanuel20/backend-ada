### Requisitos

[X] A API que provemos deve ser usada para persistência dos cards (ela trabalha com persistência em memória) e não deve ser alterada.

[X] A interface gráfica será apenas uma tela, nela deve haver três colunas chamadas "To do", "Doing" e "Done".


[X] Os cards deve ser listados nessas colunas de acordo com o valor do campo lista presente no card. Os valores de lista devem ser "ToDo", "Doing" e "Done", respectivamente.

[ ] Deve haver um local que permita criar um card passando valores para o titulo e conteudo, deve haver um botão para adicionar o card.

[ ] Um novo card deve sempre cair na lista "To Do" após persistido na API.

[ ] O card deverá ter dois modos: Visualização e Edição.

[ ] No modo de visualização o card terá um cabeçalho com seu título, o conteúdo e 4 botões.

[ ] O conteudo do card pode ser markdown, utilize uma biblioteca para renderizá-lo no modo de visualização (recomendamos uma combinação de dompurify e marked). Lembre-se de estilizar o html resultante do parse do markdown... [Se quiser usar highlight para campos de código no markdown será um diferencial].

[ ] Um dos botões do card deverá excluí-lo (persistindo pela API), outro colocá-lo em modo de edição.

[ ] Os dois outros botões devem mudar o card para a lista anterior (se houver) ou para a lista seguinte (se houver). A decisão de desabilitar, esconder ou apenas não gerar o evento desses botões quando não houver a proxima lista ou a anterior é sua.

[ ] No modo de edição, o card conterá um input para o titulo, um textarea para o conteudo e dois botões.

[ ] No modo de edição, um dos botões cancela a edição, quando precionado os campos devem ser resetados para o valor atual e voltar o card ao modo de visualização.

[ ] O outro botão salva o card, persistindo as informações pela API. Também volta ao modo de visualização em seguida.

[X] Toda decisão de visual, de UI e UX é sua. Apenas utilize uma única tela.

[X] Se estiver usando REACT priorize componentes funcionais e hooks.

[X] O projeto deve ser colocado em um repositório GITHUB ou equivalente, estar público, e conter um readme.md que explique em detalhes qualquer comando ou configuração necessária para fazer o projeto rodar.

[X] A entrega será apenas a URL para clonarmos o repositório.