import React, { useState } from 'react'
import ReactDOM from 'react-dom/client'
import App from './App'
import { axiosHttpClient } from './infrastructure/http/clients/axios-http-client'
import { IHttpClient } from './infrastructure/http/IHttpClient'
import { localStorageService } from './infrastructure/cache/local-storage-service'
import { ICacheService } from './infrastructure/cache/ICacheService'

interface IContext { 
  iocContainer: Record<any, any>, 
  isAuthenticated: boolean,
  setIsAuthenticated: Function
}

interface IIoCContainer { 
  httpClient: IHttpClient, 
  cacheService: ICacheService 
}

export const AppContext = React.createContext<IContext>({
  iocContainer: {},
  isAuthenticated: false,
  setIsAuthenticated: () => {}
})

const AppProvider = AppContext.Provider;

const iocContainer: IIoCContainer = {
  httpClient: axiosHttpClient,
  cacheService: localStorageService 
}

const Appplication = () => {
  const [isAuthenticated, setIsAuthenticated] = useState(false);

  return  (
    <React.StrictMode>
      <AppProvider value={{ iocContainer, isAuthenticated, setIsAuthenticated }}>
        <App />
      </AppProvider>
    </React.StrictMode>
  )
}

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(<Appplication/>)
