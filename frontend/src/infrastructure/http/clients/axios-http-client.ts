import axios from "axios";
import { IHttpClient } from "../IHttpClient";

if(localStorage.getItem('access_token')) {
  axios.defaults.headers.common['Authorization'] = `Bearer ${localStorage.getItem('access_token')}`;
}

axios.defaults.headers.common['Accept'] = 'application/json';
axios.defaults.headers.common['Content-Type'] = 'application/json';

export const axiosHttpClient: IHttpClient = {
  get: (url: string) => axios.get(url).then(({ data }) => data),
  post: (url: string, body: any) => axios.post(url, body).then(({ data }) => data),
  put: (url: string, body: any) => axios.put(url).then(({ data }) => data),
  delete: (url: string, body: any) => axios.delete(url).then(({ data }) => data)
}