export interface IHttpClient {
  get(url: string): Promise<unknown>
  post(url: string, body: any): Promise<any>
  put(url: string, body: any): Promise<any>
  delete(url: string, body: any): Promise<any>
}