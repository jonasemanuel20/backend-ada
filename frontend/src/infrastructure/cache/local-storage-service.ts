import { ICacheService } from "./ICacheService";

export const localStorageService: ICacheService = {
  get: function (key: string) {
    return localStorage.getItem(key)
  },
  set: function (key: string, value: string) {
    localStorage.setItem(key, value)
  },
  remove(key) {
    localStorage.removeItem(key)
  },
}