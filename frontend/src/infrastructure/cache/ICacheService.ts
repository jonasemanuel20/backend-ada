export interface ICacheService {
  get(key: string): any
  set(key: string, value: string): any
  remove(key: string): any
}