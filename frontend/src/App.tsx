import { useState } from 'react'
import { Board } from './components/Board/Board'
import { Card } from './components/Card'
import { Column } from './components/Column'
import { CardDialog } from './components/CardDialog/CardDialog'

import { useCards } from './hooks/useCards'
import { useAuthentication } from './hooks/useAuthentication'
import { ICard } from './types/ICard'

function App() {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [selectedCard, setSelectedCard] = useState<ICard>();

  const boardColumns = [
    { name: 'To do', color: '#E1E4E8' }, 
    { name: 'Doing', color: '#F0E7F6' }, 
    { name: 'Done', color: '#a2f04e' }
  ]

  useAuthentication({ login: 'letscode', password: 'lets@123' })
  const { cards, createOrUpdate, remove } = useCards(); 

  const renderCards = (cards: ICard[], columnName: string) => {
    const filteredCards = cards.filter(card => card.listName === columnName)
    return (
      <>
        {filteredCards.map(card => (
          <Card
            cardInfo={card}
            onClick={() => {
              setSelectedCard(card)
              setIsModalOpen(true)
            }}
          />)
        )}
      </>
    )
  }

  return (
    <div className="App">
      {isModalOpen && <CardDialog
        id={selectedCard?.id}
        title={selectedCard?.title}
        content={selectedCard?.content}
        listName={selectedCard?.listName}
        isOpen={isModalOpen}
        onClose={() => {
          setIsModalOpen(false)
          setSelectedCard(undefined)
        }}
        onSubmit={(card) => {
          createOrUpdate(card)
          setSelectedCard(undefined)
          setIsModalOpen(false)
        }}
        onDelete={card => {
          remove(card)
          setSelectedCard(undefined)
          setIsModalOpen(false)
        }}
      /> }
      <Board
        title='Desafio ADA' 
        description='Desafio técnico frontend ADA' 
        onClickNewCardButton={() => {
          setIsModalOpen(true)
        }
      }>
        {boardColumns.map(column => (<Column name={column.name} color={column.color}>{renderCards(cards, column.name)}</Column>))}
      </Board>
    </div>
  )
}

export default App
