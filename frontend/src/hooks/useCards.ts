import { useContext, useEffect, useState } from "react"
import { AppContext } from "../main";
import { ICard } from "../types/ICard";

export const useCards = () => {
  const BASE_URL = 'http://localhost:5000'
  const [cards, setCards] = useState<ICard[]>([]);
  const { iocContainer: { httpClient }, isAuthenticated } = useContext(AppContext);

  const fetchCards = () => {
    httpClient.get(`${BASE_URL}/cards`).then((data: any) => {
      const parsedCards = data.map((card: any) => ({
        id: card.id,
        title: card.titulo, 
        content: card.conteudo,
        listName: card.lista
      }))
      setCards(parsedCards)
    })
  }

  useEffect(() => {
    if(isAuthenticated) {
      fetchCards();
    }
  }, [isAuthenticated])

  const createOrUpdate = (card: ICard) => {
    const httpMethod = card.id ? 'put' : 'post'
    const cardToCreateOrUUpdate = {
      id: card.id,
      titulo: card.title, 
      conteudo: card.content, 
      lista: card.listName || 'To do'
    }
    httpClient[httpMethod](`${BASE_URL}/cards`, cardToCreateOrUUpdate).then(() => {
      fetchCards();
    })
  }

  const remove = (card: ICard) => {
    const cardToDelete = {
      id: card.id,
      titulo: card.title, 
      conteudo: card.content, 
      lista: card.listName || 'To do'
    }
    httpClient.delete(`${BASE_URL}/cards/${card.id}`, cardToDelete).then(() => {
      fetchCards();
    })
  }

  return { cards, createOrUpdate, remove }
}