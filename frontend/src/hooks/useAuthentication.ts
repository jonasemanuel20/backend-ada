import { useContext, useEffect } from "react";
import { AppContext } from "../main";

export const useAuthentication = ({ login, password }: { login: string; password: string }) => {
  const BASE_URL = 'http://localhost:5000'
  const { iocContainer: { httpClient, cacheService }, setIsAuthenticated } = useContext(AppContext);

  useEffect(() => {
    httpClient.post(`${BASE_URL}/login`, { login, senha: password }).then((data: any) => {
      cacheService.set('access_token', data)
      setIsAuthenticated(true)
    }).catch(() => {
      cacheService.remove('access_token')
      setIsAuthenticated(false)
    })
  }, [])
}