import styled from 'styled-components';

export const ColumnStyled = styled.div<{ color: string }>`
  padding: 10px;
  
  .column {
    &-name {
      font-size: 14px;
      background: ${props => props.color};
      border-radius: 20px;
      display: flex;
      flex-direction: column;
      align-items: center;
      padding: 4px 12px;
      font-weight: 500;
      width: max-content;
      color: #4D4D4D;
      letter-spacing: 0.5px;
      margin-bottom: 20px;
    }

    &-cards {
      div {
        margin-bottom: 15px;
      }
    }
  }
`