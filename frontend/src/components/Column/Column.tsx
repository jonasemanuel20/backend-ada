import { ReactElement, useId } from "react"
import { ColumnStyled } from "./ColumnStyled"
import { EmptyColumn } from "../EmptyColumn/EmptyColumn"

export interface IColumnProps {
  children: ReactElement
  name: string,
  color?: string
}

export const Column = ({ children, name, color="#E1E4E8" }: IColumnProps) => {
  return (
    <ColumnStyled color={color}>
      <div className="column-name">{name}</div>
      <div className="column-cards">
        {children.props.children.length > 0 ? children : <EmptyColumn message="Empty column"/>}
      </div>
    </ColumnStyled>
  )
}