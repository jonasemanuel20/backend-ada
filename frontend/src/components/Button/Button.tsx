import { ButtonStyled } from "./ButtonStyled"

export interface IButtonProps { 
  label: string; 
  onClick?: () => void; 
  type?: 'primary' | 'danger' | 'success' | 'error';
  size?: string;
}

export const Button = ({ label, onClick, type = 'primary', size = 'medium' }: IButtonProps) => {
  const getColorByType: Record<string, string> = {
    primary: '#a2f04e',
    danger: '#f8e962',
    success: '#a1c98d',
    error: '#ff6767'
  }

  const getButtonSizeBySize: Record<string, { height: string; width: string }> = {
    medium: { height: '50px;', width: '170px;' },
    small: { height: '40px;', width: '120px;' }
  }

  return (
    <ButtonStyled 
      onClick={onClick} 
      color={getColorByType[type]}
      size={getButtonSizeBySize[size]}>
        {label}
    </ButtonStyled>
  )
}