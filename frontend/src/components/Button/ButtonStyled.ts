import styled from "styled-components";

export const ButtonStyled = styled.button<{ size: { height: string; width: string }, color: string }>`
  background: ${props => props.color};
  border: 1px solid #dddddd85;
  box-shadow: 0px 2px 4px rgb(0 0 0 / 8%);
  border-radius: 8px;
  width: ${props => props.size.width}
  height: ${props => props.size.height}
  font-size: 16px;
  font-weight: 600;
  cursor: pointer;
  user-select: none;
  transition: transform 0.2s;

  :hover {
    transform: scale(1.05);
  }
`