import styled from 'styled-components';

export const BoardStyled = styled.div`
  width: 100%;
  height: 90vh;

  .board {
    &-header {
      display: flex;
      align-items: center;
      justify-content: space-between;
      padding-bottom: 30px;
    }

    &-info {
      h1 {
        margin: 0;
      }

      h2 {
        font-size: 16px;
        color: #5A5A65;
        padding: 0;
        margin: 0;
        font-weight: 500;
      }
    }

    &-columns {
      display: flex;
      flex-direction: row;
      width: 100%;
      height: 80%;
      padding: 15px;
      box-sizing: border-box;
      background: #F8F8F8;
      border-radius: 12px;
    }

    &-column {
      min-width: 270px;
      margin-right: 15px;
      overflow: auto;
    }
   }
`