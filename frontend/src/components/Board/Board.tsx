import { ReactElement, useId } from "react";

import { BoardStyled } from "./BoardStyled"
import { Button } from "../Button";

export interface IBoardProps {
  children?: ReactElement[],
  title: string; 
  description: string;
  onClickNewCardButton: () => void;
}

export const Board = ({ children, title, description, onClickNewCardButton }: IBoardProps) => {
  return (
    <BoardStyled>
      <div className="board-header">
        <div className="board-info">
          <h1>{title}</h1>
          <h2>{description}</h2>
        </div>
        <div className="board-actions">
          <Button label="Novo Card" onClick={onClickNewCardButton}/>
        </div>
      </div>
      <div className="board-columns">
        {children?.map(child => (
          <div className="board-column" key={useId()}>
            {child}
          </div>
        ))}
      </div>
    </BoardStyled>
  )
}