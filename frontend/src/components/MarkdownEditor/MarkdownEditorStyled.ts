import styled from "styled-components";

export const MarkdownStyled = styled.div`
  .w-md-editor {
    background-color: #f0f0f0;
    color: #000000;

    .wmde-markdown {
      background-color: #f0f0f0;
    }

    .wmde-markdown-color {
      color: #000000;
    }
  }
`