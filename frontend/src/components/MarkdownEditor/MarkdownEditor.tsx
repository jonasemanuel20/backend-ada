import MDEditor from "@uiw/react-md-editor";
import { useState } from "react";
import { MarkdownStyled } from "./MarkdownEditorStyled";

export const MarkdownEditor = ({ content = "", onChange }: { content?: string, onChange?: (value: any) => void }) => {
  const [value, setValue] = useState<string | undefined>(content);
  return (
    <MarkdownStyled>
      <MDEditor
        preview="edit"
        value={value}
        onChange={(value) => { 
          setValue(value)
          onChange?.(value)
        }}
      />
    </MarkdownStyled>
  )
}