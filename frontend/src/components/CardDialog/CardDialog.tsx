import { CardDialogStyled } from "./CardDialogStyled"
import { MarkdownEditor } from "../MarkdownEditor";
import { Button } from "../Button";
import { useState } from "react";
import { CloseIcon } from "../Icons";

export interface ICardDialogProps { 
  id?: string;
  title?: string; 
  content?: string;
  listName?: string;
  isOpen: boolean; 
  onSubmit: (card: any) => void,
  onClose: () => void,
  onDelete: (card: any) => void
}

export const CardDialog = ({ 
  id, 
  title, 
  content, 
  listName, 
  isOpen, 
  onSubmit, 
  onClose,
  onDelete
}: ICardDialogProps) => {
  const [card, setCard] = useState({ id, title, content, listName })

  console.log(card)

  return (
    <CardDialogStyled>
      <dialog open={isOpen}>
        <div className="dialog-close-button">
          <CloseIcon onClick={onClose} />
        </div>
        <div className="dialog-title">
          <h1>{title ? "Atualizar Cartão": "Novo Cartão"}</h1>
        </div>
        <div className="dialog-content">
          <div className="dialog-title-input">
            <div>Nome do Cartão: </div>
            <input type="text" value={card.title} onChange={(e) => setCard({ ...card, title: e.currentTarget.value })}/>
          </div>
          <div className="dialog-description-title">Descrição do cartão:</div>
          <MarkdownEditor  content={content} onChange={value => {
            setCard({ ...card, content: value })
          }} />
        </div>
        <div className="dialog-actions">
          {<Button size="small" label={id ? 'Salvar' : 'Criar'} onClick={() => onSubmit(card)} />}
          {id && <Button size="small" type="error" label="Excluir" onClick={() => onDelete(card)} />}
        </div>
      </dialog>
    </CardDialogStyled>
  )
}