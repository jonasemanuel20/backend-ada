import styled from "styled-components";

export const CardDialogStyled = styled.div`
  dialog {
    background: #ffffff;
    border-radius: 0.5em;
    box-shadow: 0 10px 20px rgba(black, 0.2);
    pointer-events: none;
    top: 35%;
    transform: translate(-50%, -50%);
    left: 30%;
    width: 30vw;
    border: 1px solid #00000036;
  }

  .dialog {
    &-close-button {
      align-items: center;
      color: #111827;
      display: flex;
      height: 4.5em;
      justify-content: center;
      pointer-events: none;
      position: absolute;
      right: 0;
      top: 0;
      width: 4.5em;
      cursor: pointer;
      pointer-events: all;

      svg {
        display: block;
      }
    }

    &-title {
      color: #111827;
      pointer-events: all;
      position: relative;
      width: calc(100% - 4.5em);

      h1 {
        font-size: 1.25rem;
        font-weight: 600;
        line-height: normal;
      }
    }

    &-content {
      border-top: 1px solid #e0e0e0;
      pointer-events: all;
      overflow: auto;
      margin-bottom: 15px;
      margin-top: 15px;
    }

    &-title-input {
      display: flex;
      margin: 20px 0;
      align-items: center;

      div {
        font-size: 14px;
      }

      input {
        margin-left: 10px;
        padding: 10px;
        border: 1px solid #0000002e;
        border-radius: 5px;
      }
    }

    &-description-title {
      margin-bottom: 5px;
      font-size: 14px;
    }

    &-actions {
      pointer-events: all;

      button {
        margin-right: 10px;
      }
    }
  }
`