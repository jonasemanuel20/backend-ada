import styled from 'styled-components';

export const CardStyled = styled.div`
  background: #FFFFFF;
  border: 1px solid #dddddd85;
  box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.08);
  border-radius: 8px;
  padding: 16px;
  height: 90px;
  cursor: pointer;
  user-select: none;
  transition: transform 0.2s;

  :hover {
    transform: scale(1.05);
  }

  h3 {
    font-size: 16px;
    margin: 0;
  }

  .card {
    &-description {
      font-size: 14px;
      color: #5A5A65;
      font-weight: 400;
    }
  }
`