import { CardStyled } from "./CardStyled"

export interface ICardProps { 
  cardInfo: {
    title: string; 
    content: string;
  }, 
  onClick: () => void 
}

export const Card = ({ cardInfo: { title, content }, onClick }: ICardProps) => {
  return (
    <CardStyled onClick={onClick}>
      <h3>{title}</h3>
      <div className="card-description">{content}</div>
    </CardStyled>
  )
}