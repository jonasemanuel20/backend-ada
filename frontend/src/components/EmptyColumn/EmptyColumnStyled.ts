import styled from 'styled-components';

export const EmptyColumnStyled = styled.div`
  background: #FFFFFF;
  border: 1px solid #dddddd85;
  box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.08);
  border-radius: 8px;
  padding: 16px;
  height: 90px;
  cursor: pointer;
  user-select: none;
  border-style: dashed;
  border-color: #0000001a;
  display: flex;
  align-items: center;
  justify-content: center;
`