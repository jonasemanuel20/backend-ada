import { EmptyColumnStyled } from "./EmptyColumnStyled"

export interface IEmptyColumnProps {
  message: string;
}

export const EmptyColumn = ({ message }: IEmptyColumnProps) => {
  return (
    <EmptyColumnStyled>
      {message}
    </EmptyColumnStyled>
  )
}