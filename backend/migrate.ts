import fs from 'node:fs'
import { unlink } from 'node:fs/promises';
import sqlite3 from 'sqlite3';

(async () => {
  await unlink('./database.sqlite')
  fs.appendFileSync('./database.sqlite', '')
  const db = new sqlite3.Database('./database.sqlite');
  db.serialize(() => {
    db.run(`
      CREATE TABLE cards (
        id TEXT PRIMARY KEY, 
        title TEXT NOT NULL, 
        content TEXT NOT NULL,
        listName TEXT NOT NULL,
        createdAt DATETIME,
        updatedAt DATETIME
      );
    `)
  })
  db.close();
})()

