import { EntityError } from "../../errors/EntityError";

export class Card {
  readonly id: string;
  readonly title: string;
  readonly content: string;
  readonly listName: string;

  constructor(id: string, title: string, content: string, listName: string) {
    if(!title) {
      throw new EntityError("Card must have a title")
    }
    if(!content) {
      throw new EntityError("Card must have a content")
    }
    if(!listName) {
      throw new EntityError("Card must be in a list")
    }
    this.id = id;
    this.title = title;
    this.content = content;
    this.listName = listName;
  }
}