import { test, expect } from "vitest";
import { Card } from "./Card";

const mockCardId = 'card-id';
const mockCardTitle = 'card-title';
const mockCardContent = 'card-content';
const mockCardListName = 'card-list-name';

test('should not instance a Card without title', () => {
  const createCard = () => new Card(mockCardId, "", mockCardContent, mockCardListName)
  expect(createCard).toThrow("Card must have a title")
})

test('should not instance a Card without content', () => {
  const createCard = () => new Card(mockCardId, mockCardTitle, "", mockCardListName)
  expect(createCard).toThrow("Card must have a content")
})

test('should not instance a Card without list name', () => {
  const createCard = () => new Card(mockCardId, mockCardTitle, mockCardContent, "")
  expect(createCard).toThrow("Card must be in a list")
})