import { HttpServer } from "./infrastructure/http/rest/Server";

const httpServer = new HttpServer();

httpServer.initDatabase();
httpServer.initMiddlewares();
httpServer.initRouter();
httpServer.addErrorHandler();

httpServer.listen(3000);