import { CreateCard, DeleteCard } from "../../../../application/usecases";
import { GetAllCards } from "../../../../application/usecases/GetAllCards/GetAllCards";
import { UpdateCard } from "../../../../application/usecases/UpdateCard/UpdateCard";

export class CardController {
  constructor(private readonly cardRepository) {}

  async getAll(request, response) {
    const getAllCards = new GetAllCards(this.cardRepository)
    const cards = await getAllCards.execute(request.query);
    response.json(cards);
  }

  async get(request, response) {}

  async save(request, response, next) {
    const createCard = new CreateCard(this.cardRepository);
    const card = createCard.execute(request.body)
    response.json(card)
  }

  async update(request, response) {
    const updateCard = new UpdateCard(this.cardRepository);
    const card = updateCard.execute(request.body)
    response.json(card)
  }

  async delete(request, response) {
    const deleteCard = new DeleteCard(this.cardRepository);
    const cards = await deleteCard.execute(request.params.id)
    response.json(cards)
  }
}