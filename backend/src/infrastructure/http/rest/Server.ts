import express, { Router } from "express";
import { Sequelize } from "sequelize";
import { errorHandler } from "./middlewares/errorHandler";
import { CardRouter } from "./routes/CardRouter";

export class HttpServer {
  application: any;
  applicationRouter: Router;

  constructor() {
    this.application = express();
    this.applicationRouter = express.Router();
  }

  async initDatabase() {
    const sequelize = new Sequelize({
      dialect: 'sqlite',
      storage: `database.sqlite`
    });

    try {
      await sequelize.authenticate();
      console.log('Connection has been established successfully.');
    } catch (error) {
      console.error('Unable to connect to the database:', error);
    }    
  }

  initMiddlewares() {
    this.application.use(express.json())
  }

  initRouter() {
    this.application.use('/cards', new CardRouter())
  }

  addErrorHandler() {
    // this.application.use((request, response, next) => {
    //   process.on('uncaughtException', (error: Error) => {
    //     console.log("JONAS")
    //   });
    //   next()
    // })
  }

  listen(port: number) {
    this.application.listen(port, () => {
      console.log(`Application listening on ${port}`)
    });
  }
}