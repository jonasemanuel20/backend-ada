import express from "express";
import { CardSQLiteRepository } from "../../../data/repositories/implementations/CardSQLiteRepository";
import { CardController } from "../controllers/CardController";

export class CardRouter {
  constructor() {
    const cardController = new CardController(new CardSQLiteRepository());
    const router =  express.Router();

    router.get('/', cardController.getAll.bind(cardController))
    router.post('/', cardController.save.bind(cardController))
    router.put('/:id', cardController.update.bind(cardController))
    router.delete('/:id', cardController.delete.bind(cardController))

    return router;
  }
}