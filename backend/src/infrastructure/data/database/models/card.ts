import { DataTypes, Sequelize } from "sequelize";

const sequelize = new Sequelize({
  dialect: 'sqlite',
  storage: `database.sqlite`
});

export const CardModel = sequelize.define('card', {
  id: {
    type: DataTypes.STRING,
    primaryKey: true
  },
  title: DataTypes.STRING,
  content: DataTypes.STRING,
  listName: DataTypes.STRING
})