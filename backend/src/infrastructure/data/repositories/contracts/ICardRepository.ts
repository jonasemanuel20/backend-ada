import { UpdateCardInputDTO } from "../../../../application/dtos/UpdateCardInputDTO";
import { Card } from "../../../../domain/entities/Card";

export interface ICardRepository {
  findAll(filters?: Record<any, any>): Promise<Card[]>;
  save(card: Card): Promise<Card>;
  update(card: UpdateCardInputDTO): Promise<Card>;
  remove(id: string): Promise<Card[]>;
}