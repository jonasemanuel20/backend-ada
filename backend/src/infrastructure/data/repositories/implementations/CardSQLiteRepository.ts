import { UpdateCardInputDTO } from "../../../../application/dtos/UpdateCardInputDTO";
import { Card } from "../../../../domain/entities/Card";
import { CardModel } from "../../database/models/card";
import { ICardRepository } from "../contracts/ICardRepository";

export class CardSQLiteRepository implements ICardRepository {

  constructor() {}

  async findAll(filters = {}): Promise<any> {
    const cards = await CardModel.findAll({ where: filters });
    return cards;
  }

  async save(card: Card): Promise<Card> {
    const model = CardModel.build({
      id: card.id,
      title: card.title,
      content: card.content,
      listName: card.listName
    });

    await model.save()

    return {
      id: card.id,
      title: card.title,
      content: card.content,
      listName: card.listName
    }
  }
  async update(card: UpdateCardInputDTO): Promise<Card> {
    //todo
    const model = CardModel.build({
      id: card.id,
      title: card.title,
      content: card.content,
      listName: card.listName,
      updatedAt: card.updatedAt,
      createdAt: card.createdAt
    });

  console.log(model)

    await model.update(model)

    return card;
  }
  
  async remove(id: string): Promise<Card[]> {
    const model = await CardModel.destroy({ where: { id }})
    const cards = await this.findAll();
    return cards;
  }
}