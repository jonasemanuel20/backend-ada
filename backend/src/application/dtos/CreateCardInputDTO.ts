export interface CreateCardInputDTO {
  title: string;
  content: string;
  listName: string;
}