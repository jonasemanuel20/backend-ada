export interface CreateCardOutputDTO {
  id: string;
  title: string;
  content: string;
  listName: string;
}