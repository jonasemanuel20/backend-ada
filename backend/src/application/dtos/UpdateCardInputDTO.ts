export interface UpdateCardInputDTO {
  id: string;
  title: string;
  content: string;
  listName: string;
  updatedAt: string;
  createdAt: string;
}