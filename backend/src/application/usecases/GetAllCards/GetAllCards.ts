import { ICardRepository } from '../../../infrastructure/data/repositories/contracts/ICardRepository';

export class GetAllCards {
  constructor(readonly cardRepository: ICardRepository) { }

  async execute(filters?: Record<any, any>): Promise<any> {
    return await this.cardRepository.findAll(filters)
  }
} 