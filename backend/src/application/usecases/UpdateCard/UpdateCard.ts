import { Card } from '../../../domain/entities/Card';
import { ICardRepository } from '../../../infrastructure/data/repositories/contracts/ICardRepository';
import { UpdateCardInputDTO } from '../../dtos/UpdateCardInputDTO';

export class UpdateCard {
  constructor(readonly cardRepository: ICardRepository) { }

  async execute(input: UpdateCardInputDTO): Promise<any> {
    const card = new Card(input.id, input.title, input.content, input.listName)
    return await this.cardRepository.update({
      ...card,
      updatedAt: input.updatedAt,
      createdAt: input.createdAt,
    })
  }
} 