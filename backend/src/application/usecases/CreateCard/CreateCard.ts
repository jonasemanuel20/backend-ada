import { v4 as uuidv4 } from 'uuid';
import { Card } from '../../../domain/entities/Card';
import { ICardRepository } from '../../../infrastructure/data/repositories/contracts/ICardRepository';
import { CreateCardInputDTO } from '../../dtos/CreateCardInputDTO';
import { CreateCardOutputDTO } from '../../dtos/CreateCardOutputDTO';

export class CreateCard {
  constructor(readonly cardRepository: ICardRepository) { }

  async execute(input: CreateCardInputDTO): Promise<CreateCardOutputDTO> {
    const id = uuidv4();
    const card = new Card(id, input.title, input.content, input.listName)
    await this.cardRepository.save(card)
    return {
      id,
      ...input,
    }
  }
} 