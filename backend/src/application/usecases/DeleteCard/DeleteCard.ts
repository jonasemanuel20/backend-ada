import { ICardRepository } from '../../../infrastructure/data/repositories/contracts/ICardRepository';

export class DeleteCard {
  constructor(readonly cardRepository: ICardRepository) { }

  async execute(id: string): Promise<any> {
    return await this.cardRepository.remove(id)
  }
} 